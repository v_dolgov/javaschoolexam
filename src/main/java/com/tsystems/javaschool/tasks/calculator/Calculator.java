package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;
import java.util.StringTokenizer;

public class Calculator {

    final String operators = "+-/*";

    /**
     * Marks out operators with blank spaces to simplify parsing
     * @param input     given string
     * @return          processed input string
     */
    String preprocess (String input){
        String mOperators = "+-/*()";
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < input.length(); i++){
            if (mOperators.contains( Character.toString(input.charAt(i)) )){
                result.append(" "+input.charAt(i) + " ");
            }
            else    {
                result.append(input.charAt(i));
            }
        }

        return result.toString();
    }

    /**
     * Transforms given arithmetic expression to postfix notation
     * @param input     given string
     * @return          postfix output
     */
    String toPostfix (String input) {

        StringBuilder str = new StringBuilder();
        Stack<Integer> stack = new Stack<>();

        for (String token : input.split("\\s")) {
            if (token.isEmpty())
                continue;
            char c = token.charAt(0);
            int index = operators.indexOf(c);

            if (index != -1) {
                if (stack.isEmpty())
                    stack.push(index);
                else {
                    while (!stack.isEmpty()) {
                        int prec2 = stack.peek() / 2;
                        int prec1 = index / 2;
                        if (prec2 > prec1 || (prec2 == prec1))
                            str.append(operators.charAt(stack.pop())).append(' ');
                        else break;
                    }
                    stack.push(index);
                }
            }
            else if (c == '(') {
                stack.push(-2); // -2 stands for '('
            }
            else if (c == ')') {
                while (stack.peek() != -2)
                    str.append(operators.charAt(stack.pop())).append(' ');
                stack.pop();
            }
            else {
                str.append(token).append(' ');
            }
        }

        while (!stack.isEmpty())
            str.append(operators.charAt(stack.pop())).append(' ');

        return str.toString();
    }


    public String evaluate(String statement) {
        try {
            statement = toPostfix(preprocess(statement));
            toPostfix(preprocess(statement));
            StringTokenizer tokenizer = new StringTokenizer(statement);
            Stack<Double> stack = new Stack<>();
            while (tokenizer.hasMoreTokens()) {
                String s = tokenizer.nextToken();
                switch (s) {
                    case "+":
                        stack.push(stack.pop() + stack.pop());
                        break;
                    case "-":
                        double a = stack.pop();
                        double b = stack.pop();
                        stack.push(b - a);
                        break;
                    case "*":
                        stack.push(stack.pop() * stack.pop());
                        break;
                    case "/":
                        a = stack.pop();
                        b = stack.pop();
                        if (a == 0) {
                            throw new ArithmeticException();
                        }
                        stack.push(b / a);
                        break;
                    default:
                        stack.push(Double.parseDouble(s));
                        break;
                }
            }
            Double res = stack.pop();

            if (res % 1 != 0) {
                res = Math.floor(res * 10000) / 10000;
                return Double.toString(res);
            }
            else {
                return Integer.toString(res.intValue());
            }
        }
        catch (Exception e) {
            return null;
        }

    }

}
