package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        // TODO : Implement your solution here
        if (inputNumbers.size() > 10000)
            throw new CannotBuildPyramidException("Input is too big");
        if (inputNumbers.contains(null))
            throw new CannotBuildPyramidException("null");
        else
            Collections.sort(inputNumbers);

        int level = 0;
        int levelSize = 0;
        int step = 1;
        boolean pyramidIsPossible = false;

        for (int x : inputNumbers){
            levelSize += step;
            level++;
            if (levelSize == inputNumbers.size()) {
                pyramidIsPossible = true;
                break;
            }
            step++;
        }

        if (!pyramidIsPossible)
            throw new CannotBuildPyramidException("Input doesn't fit for pyramid building");

        int height = level;
        int width = level * 2 - 1;
        int[][] output = new int[height][width];
        int pointer = inputNumbers.size() - 1;
        for (int i = height - 1; i >= 0; i--){
            int counter = (height - 1) - i;
            int zeroesCounter = counter;
            for (int j = width - 1; j >= 0; j--){
                if ((j == width -1 - counter) && (j >= zeroesCounter)) {
                    output[i][j] = inputNumbers.get(pointer);
                    pointer--;
                    counter = counter + 2;
                }
            }
        }

        return output;
    }


}
